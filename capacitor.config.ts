import { CapacitorConfig } from '@capacitor/cli';

const server = {
  'url': 'http://192.168.1.42:8081',
  'cleartext': true
};

const config: CapacitorConfig = {
  appId: 'com.soundair.app',
  appName: 'Sound Air',
  webDir: 'dist',
  bundledWebRuntime: false,
  plugins: {
    LocalNotifications: {
      smallIcon: 'ic_stat_icon_config_sample',
      iconColor: '#488AFF',
      sound: 'beep.wav',
    },
  },
};

export default config;

